package com.citi.training.trades.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.service.TradeService;

@RestController
@RequestMapping("/trades")
public class TradesController {
	
	 @Autowired
	 private TradeService tradeService;

	    
	    @RequestMapping(method=RequestMethod.GET,
	                    produces=MediaType.APPLICATION_JSON_VALUE)
	    public List<Trade> findAll(){
	        
	        return tradeService.findAll();
	    }


	    @RequestMapping(value="/{id}", method=RequestMethod.GET,
	                    produces=MediaType.APPLICATION_JSON_VALUE)
	    public Trade findById(@PathVariable int id) {
	       
	        return tradeService.findById(id);
	    }

	    @RequestMapping(method=RequestMethod.POST,
	                    consumes=MediaType.APPLICATION_JSON_VALUE,
	                    produces=MediaType.APPLICATION_JSON_VALUE)
	    public HttpEntity<Trade> create(@RequestBody Trade Trade) {
	       
	        return new ResponseEntity<Trade>(tradeService.create(Trade),
	                                            HttpStatus.CREATED);
	    }

	
	    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	    @ResponseStatus(HttpStatus.NO_CONTENT)
	    public @ResponseBody void deleteById(@PathVariable int id) {
	       
	        tradeService.deleteById(id);
	    }

}
