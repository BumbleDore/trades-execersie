package com.citi.training.trades.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trades.dao.MySqlTradesDao;
import com.citi.training.trades.model.Trade;


@Component
public class TradeService {
	
	@Autowired
	private MySqlTradesDao tradesDao;
	
	   public List<Trade> findAll() { return tradesDao.findAll(); };

	   public Trade findById(int id) { return tradesDao.findById(id); };

	   public Trade create(Trade Trade) { 
		   
		   return tradesDao.create(Trade); 
		   
	   }

	   public void deleteById(int id) { tradesDao.deleteById(id); };

}
