package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;



public class TradeTests {
	private int testId = 53;
    private String testName = "APPL";
    private double testPrice = 53.99;
    private int testVolume = 33;
    
    @Test 
    public void test_Trade_Constructor() {
    	Trade trade = new Trade(testId, testName, testPrice, testVolume);
    	
    	assertEquals(testId, trade.getId());
    	assertEquals(testName, trade.getStock());
    	assertEquals(testVolume, trade.getVolume());
    	
    }
    
    @Test
    public void test_Trade_toString() {
        String testString = new Trade(testId, testName, testPrice, testVolume).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testName));
        assertTrue(testString.contains((new Double(testPrice)).toString()));
        assertTrue(testString.contains((new Integer(testVolume)).toString()));
        }

}
