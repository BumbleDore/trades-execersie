package com.citi.training.trades.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


import com.citi.training.trades.model.Trade;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class TradesDaoTests {
	

	@Autowired
	public MySqlTradesDao mysqlTradeDao;
	
	@Test
    @Transactional
    public void test_createAndFindAll() {
        mysqlTradeDao.create(new Trade(1, "APPL", 53.99,4));
        

        assertEquals(1, mysqlTradeDao.findAll().size());
    }
	
	 @Test
	 @Transactional
	 public void test_CreateAndFindById() {
		  Trade test = mysqlTradeDao.create(new Trade(1, "APPL", 53.99,4));
		 
		  assertThat(test).isEqualToComparingFieldByField(mysqlTradeDao.findById(1));
		  
	 }
	 
	 @Test
	 @Transactional
	 public void test_CreateAndDeleteById() {
		  Trade test = mysqlTradeDao.create(new Trade(1, "APPL", 53.99,4));
		  Trade trade =mysqlTradeDao.create(new Trade(2, "A32323", 593.33,45));
		  
		  mysqlTradeDao.deleteById(trade.getId());
		  
		  assertEquals(1, mysqlTradeDao.findAll().size());
		  assertThat(test).isEqualToComparingFieldByField(mysqlTradeDao.findById(test.getId()));
	
		  
	 }

}
